# 📕Documentation: Process
The Performed Process sub-ontology deals with Performed Processes and Activities, and how they are decomposed.

## 🌀 Package's Data Model
![Domain Diagram](classdiagram.png)

### ⚡Entities

* **GeneralProjectProcess** : Refers to the whole intented or performed process defined for a project.
* **SpecificProjectProcess** : Refers a defined and intended or performed process with a specific purpose for a project.
* **Activity** : An intended or performed activity in a process.

## ✒️ Mapping Rules
### GeneralProjectProcess
Refers to the whole intented or performed process defined for a project.

#### From generalprojectprocess to GeneralProjectProcess

* *payload.after.name* **equal** *name*
* *payload.after.description* **equal** *description*
* *payload.after.start_date* **equal** *startDate*
* *payload.after.end_date* **equal** *endDate*
* *payload.after.external_id* **equal** *externalId*
* *payload.after.internal_id* **equal** *internalId*




### SpecificProjectProcess
Refers a defined and intended or performed process with a specific purpose for a project.

#### From specificprojectprocess to SpecificProjectProcess

* *payload.after.name* **equal** *name*
* *payload.after.description* **equal** *description*
* *payload.after.start_date* **equal** *startDate*
* *payload.after.end_date* **equal** *endDate*
* *payload.after.external_id* **equal** *externalId*
* *payload.after.internal_id* **equal** *internalId*




### Activity
An intended or performed activity in a process.

#### From activity to Activity

* *payload.after.name* **equal** *name*
* *payload.after.description* **equal** *description*
* *payload.after.start_date* **equal** *startDate*
* *payload.after.end_date* **equal** *endDate*
* *payload.after.external_id* **equal** *externalId*
* *payload.after.internal_id* **equal** *internalId*




