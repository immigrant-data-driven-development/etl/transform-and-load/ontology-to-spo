# 📕Documentation: Project


## 🌀 Package's Data Model
![Domain Diagram](classdiagram.png)

### ⚡Entities

* **SoftwareProject** : A Project related to software development or maintenance.

## ✒️ Mapping Rules
### SoftwareProject
A Project related to software development or maintenance.

#### From project to SoftwareProject

* *payload.after.name* **equal** *name*
* *payload.after.description* **equal** *description*
* *payload.after.start_date* **equal** *startDate*
* *payload.after.end_date* **equal** *endDate*
* *payload.after.external_id* **equal** *externalId*
* *payload.after.internal_id* **equal** *internalId*




