# 📕Documentation

## 🌀 Project's Package Model
![Domain Diagram](packagediagram.png)

### 📲 Modules
* **[Project](./project/)** :-
* **[Process](./process/)** :The Performed Process sub-ontology deals with Performed Processes and Activities, and how they are decomposed.
* **[Stakeholder](./stakeholder/)** :Stakeholders refer to people, teams or organizations acting in or performing process activities.
* **[Artifact](./artifact/)** :Artifacts represent different types of objects produced and used in process activities.

