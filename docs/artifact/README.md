# 📕Documentation: Artifact
Artifacts represent different types of objects produced and used in process activities.

## 🌀 Package's Data Model
![Domain Diagram](classdiagram.png)

### ⚡Entities

* **Artifact** : Object intentionally made to serve a given purpose in the context of a software Project or Organization.

## ✒️ Mapping Rules
### Artifact
Object intentionally made to serve a given purpose in the context of a software Project or Organization.

#### From artifact to Artifact

* *payload.after.name* **equal** *name*
* *payload.after.description* **equal** *description*
* *payload.after.start_date* **equal** *startDate*
* *payload.after.end_date* **equal** *endDate*
* *payload.after.external_id* **equal** *externalId*
* *payload.after.internal_id* **equal** *internalId*




