# 📕Documentation: Stakeholder
Stakeholders refer to people, teams or organizations acting in or performing process activities.

## 🌀 Package's Data Model
![Domain Diagram](classdiagram.png)

### ⚡Entities

* **ProjectStakeholder** : Stakeholder interested in a particular Project.

## ✒️ Mapping Rules
### ProjectStakeholder
Stakeholder interested in a particular Project.

#### From projectstakeholder to ProjectStakeholder

* *payload.after.name* **equal** *name*
* *payload.after.description* **equal** *description*
* *payload.after.start_date* **equal** *startDate*
* *payload.after.end_date* **equal** *endDate*
* *payload.after.external_id* **equal** *externalId*
* *payload.after.internal_id* **equal** *internalId*




