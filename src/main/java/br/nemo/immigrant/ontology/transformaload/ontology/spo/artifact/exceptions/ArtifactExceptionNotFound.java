  package br.nemo.immigrant.ontology.transformaload.ontology.spo.artifact.exceptions;

  public class ArtifactExceptionNotFound extends RuntimeException{

    public ArtifactExceptionNotFound(String message) {
        super(message);
    }
}

