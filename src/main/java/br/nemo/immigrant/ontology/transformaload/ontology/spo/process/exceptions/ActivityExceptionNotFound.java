  package br.nemo.immigrant.ontology.transformaload.ontology.spo.process.exceptions;

  public class ActivityExceptionNotFound extends RuntimeException{

    public ActivityExceptionNotFound(String message) {
        super(message);
    }
}

