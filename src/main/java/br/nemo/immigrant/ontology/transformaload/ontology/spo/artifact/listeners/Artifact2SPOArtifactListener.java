
package br.nemo.immigrant.ontology.transformaload.ontology.spo.artifact.listeners;

import br.nemo.immigrant.ontology.entity.spo.*;
import br.nemo.immigrant.ontology.transformaload.ontology.spo.artifact.services.ArtifactService;
import br.nemo.immigrant.ontology.transformaload.ontology.spo.artifact.filters.Artifact2SPOArtifactFilter;
import br.nemo.immigrant.ontology.transformaload.ontology.spo.artifact.mappers.Artifact2SPOArtifactMapper;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.apache.kafka.clients.consumer.ConsumerRecord;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.stereotype.Service;
import org.springframework.kafka.core.KafkaTemplate;

@Slf4j
@RequiredArgsConstructor
//@Service
public class Artifact2SPOArtifactListener {


    @Autowired
    private Artifact2SPOArtifactFilter filter;

    @Autowired
    private Artifact2SPOArtifactMapper mapper;

    @Autowired
    private ArtifactService service;

    private final KafkaTemplate<String, String> kafkaTemplate;


    @KafkaListener(topics = "ontology.*.artifact", groupId = "artifact2spoartifact-group", concurrency = "2")
    public void consume(ConsumerRecord<String, String> payload) {

        try{


            String data = payload.value();

            if (filter.isValid(data)){

                this.service.process(payload, mapper);
            }

        }
        catch (ArtifactExceptionNotFound e){
            log.error(e.getMessage());
            kafkaTemplate.send("ontology.*", payload.value());

        }

        catch (Exception e ){
            log.error(e.getMessage());
        }
    }
}
