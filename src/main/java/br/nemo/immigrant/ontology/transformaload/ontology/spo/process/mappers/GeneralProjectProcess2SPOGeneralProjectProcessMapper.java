    package br.nemo.immigrant.ontology.transformaload.ontology.spo.process.mappers;

    import br.nemo.immigrant.ontology.entity.spo.*;
    import br.nemo.immigrant.ontology.transformaload.ontology.spo.util.Mapper;

    import org.springframework.boot.json.JsonParser;
    import org.springframework.boot.json.JsonParserFactory;
    import org.springframework.stereotype.Component;

    import com.fasterxml.jackson.databind.JsonNode;
    import com.fasterxml.jackson.databind.ObjectMapper;

    @Component
    public class GeneralProjectProcess2SPOGeneralProjectProcessMapper  implements Mapper <GeneralProjectProcess>{

    public GeneralProjectProcess map (String element) throws Exception {

        ObjectMapper objectMapper = new ObjectMapper();

        JsonNode rootNode = objectMapper.readTree(element);

        String name = rootNode.path("payload").path("after").path("name").asText();

        String description = rootNode.path("payload").path("after").path("description").asText();

        String startdate = rootNode.path("payload").path("after").path("start_date").asText();

        String enddate = rootNode.path("payload").path("after").path("end_date").asText();

        String externalid = rootNode.path("payload").path("after").path("external_id").asText();

        String internalid = rootNode.path("payload").path("after").path("internal_id").asText();


        return GeneralProjectProcess.builder().name(name).
                                               description(description).
                                               startDate(startdate).
                                               endDate(enddate).
                                               externalId(externalid).
                                               internalId(internalid).build();
    }
}
