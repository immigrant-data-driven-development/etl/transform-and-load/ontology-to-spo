
package br.nemo.immigrant.ontology.transformaload.ontology.spo.process.listeners;

import br.nemo.immigrant.ontology.entity.spo.*;
import br.nemo.immigrant.ontology.transformaload.ontology.spo.process.services.GeneralProjectProcessService;
import br.nemo.immigrant.ontology.transformaload.ontology.spo.process.filters.GeneralProjectProcess2SPOGeneralProjectProcessFilter;
import br.nemo.immigrant.ontology.transformaload.ontology.spo.process.mappers.GeneralProjectProcess2SPOGeneralProjectProcessMapper;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.apache.kafka.clients.consumer.ConsumerRecord;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.stereotype.Service;
import org.springframework.kafka.core.KafkaTemplate;

@Slf4j
@RequiredArgsConstructor
//@Service
public class GeneralProjectProcess2SPOGeneralProjectProcessListener {


    @Autowired
    private GeneralProjectProcess2SPOGeneralProjectProcessFilter filter;

    @Autowired
    private GeneralProjectProcess2SPOGeneralProjectProcessMapper mapper;

    @Autowired
    private GeneralProjectProcessService service;

    private final KafkaTemplate<String, String> kafkaTemplate;


    @KafkaListener(topics = "ontology.*.generalprojectprocess", groupId = "generalprojectprocess2spogeneralprojectprocess-group", concurrency = "2")
    public void consume(ConsumerRecord<String, String> payload) {

        try{


            String data = payload.value();

            if (filter.isValid(data)){

                this.service.process(payload, mapper);
            }

        }
        catch (SoftwareProjectExceptionNotFound e){
            log.error(e.getMessage());
            kafkaTemplate.send("ontology.*", payload.value());

        }

        catch (SpecificProjectProcessExceptionNotFound e){
            log.error(e.getMessage());
            kafkaTemplate.send("ontology.*", payload.value());

        }

        catch (GeneralProjectProcessExceptionNotFound e){
            log.error(e.getMessage());
            kafkaTemplate.send("ontology.*", payload.value());

        }

        catch (Exception e ){
            log.error(e.getMessage());
        }
    }
}
