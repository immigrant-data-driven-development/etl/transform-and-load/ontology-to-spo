  package br.nemo.immigrant.ontology.transformaload.ontology.spo.process.exceptions;

  public class SpecificProjectProcessExceptionNotFound extends RuntimeException{

    public SpecificProjectProcessExceptionNotFound(String message) {
        super(message);
    }
}

