
package br.nemo.immigrant.ontology.transformaload.ontology.spo.artifact.services;

import br.nemo.immigrant.ontology.entity.spo.*;
import br.nemo.immigrant.ontology.transformaload.ontology.spo.artifact.applications.ArtifactApplication;
import br.nemo.immigrant.ontology.transformaload.ontology.spo.artifact.mappers.Artifact2SPOArtifactMapper;
import br.nemo.immigrant.ontology.transformaload.ontology.spo.util.Mapper;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.apache.kafka.clients.consumer.ConsumerRecord;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

@Slf4j
@RequiredArgsConstructor
@Component
public class ArtifactService {


    @Autowired
    private ArtifactApplication application;

    public void process(ConsumerRecord<String, String> payload,Mapper<Artifact> mapper) throws Exception, ArtifactExceptionNotFound{


        Artifact instance = mapper.map(payload.value());
        Boolean exists = application.exists(instance.getInternalId());
        if (!exists){
            application.create (instance, payload.value());

        }

    }

}
