  package br.nemo.immigrant.ontology.transformaload.ontology.spo.process.exceptions;

  public class GeneralProjectProcessExceptionNotFound extends RuntimeException{

    public GeneralProjectProcessExceptionNotFound(String message) {
        super(message);
    }
}

