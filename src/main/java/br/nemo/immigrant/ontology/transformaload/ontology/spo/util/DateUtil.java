  package br.nemo.immigrant.ontology.transformaload.ontology.spo.util;

  import java.time.LocalDate;
  import java.time.LocalDateTime;
  import java.time.format.DateTimeFormatter;
  import java.time.LocalDateTime;
  import java.time.ZonedDateTime;
  public class DateUtil {


    public  static LocalDate createLocalDate(String day, String month, String year){

        month = checkMonthAndDay(month);
        day = checkMonthAndDay(day);

        String date = year+"-"+month+"-"+day;
        return  LocalDate.parse(date);


    }

    public static LocalDateTime createLocalDateTime (String date) {
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd'T'HH:mm:ss.SSX");

        return  LocalDateTime.parse(date, formatter);
    }

    public static LocalDateTime createLocalDateTimeZ (String date) {
        DateTimeFormatter formatter = DateTimeFormatter.ISO_DATE_TIME;
        ZonedDateTime zonedDateTime = ZonedDateTime.parse(date, formatter);
        return zonedDateTime.toLocalDateTime();


    }

    private static String checkMonthAndDay(String value){
        return value.length() == 2? value: "0"+value;
    }
}
