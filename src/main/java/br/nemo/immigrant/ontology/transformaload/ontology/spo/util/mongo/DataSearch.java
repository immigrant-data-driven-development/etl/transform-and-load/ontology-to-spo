package br.nemo.immigrant.ontology.transformaload.ontology.spo.util.mongo;

import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.AllArgsConstructor;
import lombok.experimental.SuperBuilder;

@Data
@SuperBuilder
@NoArgsConstructor
@AllArgsConstructor
public class DataSearch {

    private Long elementValue;

    private String table;

    private String database;

    public String toString (){

        return "elementValue: "+elementValue + " - Table:"+table+" - Database: "+database;

    }
}
