
package br.nemo.immigrant.ontology.transformaload.ontology.spo.process.listeners.mongo;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.apache.kafka.clients.consumer.ConsumerRecord;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.stereotype.Service;
import org.springframework.kafka.core.KafkaTemplate;

@Slf4j
@RequiredArgsConstructor
@Service
public class SpecificProjectProcessInputListener {

    @Autowired
    private MongoTemplate mongoTemplate;

    @KafkaListener(topicPattern = "ontology.*.specificprojectprocess", groupId = "specificprojectprocessInput-spo-group", concurrency = "2")
    public void consume(ConsumerRecord<String, String> payload) {

        try{

            String data = payload.value();
            Document doc = Document.parse (data);
            mongoTemplate.insert(doc, "input");

        }catch (Exception e ){
            log.error(e.getMessage());
        }
    }
}

