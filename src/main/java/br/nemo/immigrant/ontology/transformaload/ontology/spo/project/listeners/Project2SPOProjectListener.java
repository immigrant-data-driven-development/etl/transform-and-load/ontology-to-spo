
package br.nemo.immigrant.ontology.transformaload.ontology.spo.project.listeners;

import br.nemo.immigrant.ontology.entity.spo.*;
import br.nemo.immigrant.ontology.transformaload.ontology.spo.project.services.SoftwareProjectService;
import br.nemo.immigrant.ontology.transformaload.ontology.spo.project.filters.Project2SPOProjectFilter;
import br.nemo.immigrant.ontology.transformaload.ontology.spo.project.mappers.Project2SPOProjectMapper;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.apache.kafka.clients.consumer.ConsumerRecord;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.stereotype.Service;
import org.springframework.kafka.core.KafkaTemplate;

@Slf4j
@RequiredArgsConstructor
//@Service
public class Project2SPOProjectListener {


    @Autowired
    private Project2SPOProjectFilter filter;

    @Autowired
    private Project2SPOProjectMapper mapper;

    @Autowired
    private SoftwareProjectService service;

    private final KafkaTemplate<String, String> kafkaTemplate;


    @KafkaListener(topics = "ontology.*.project", groupId = "project2spoproject-group", concurrency = "2")
    public void consume(ConsumerRecord<String, String> payload) {

        try{


            String data = payload.value();

            if (filter.isValid(data)){

                this.service.process(payload, mapper);
            }

        }

        catch (Exception e ){
            log.error(e.getMessage());
        }
    }
}
