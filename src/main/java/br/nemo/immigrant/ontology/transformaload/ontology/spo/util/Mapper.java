package br.nemo.immigrant.ontology.transformaload.ontology.spo.util;

public interface Mapper <T> {
  public T map (String element) throws Exception;
}
