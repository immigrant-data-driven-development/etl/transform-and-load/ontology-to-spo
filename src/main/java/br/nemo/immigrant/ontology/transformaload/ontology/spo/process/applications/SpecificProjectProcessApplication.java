  package br.nemo.immigrant.ontology.transformaload.ontology.spo.process.applications;

  import br.nemo.immigrant.ontology.transformaload.ontology.spo.util.ApplicationAbstract;
  import org.springframework.stereotype.Component;
  import org.springframework.transaction.annotation.Transactional;
  import java.util.Optional;
  import org.springframework.beans.factory.annotation.Autowired;
  import org.bson.Document;
  import br.nemo.immigrant.ontology.entity.eo.teams.repositories.projections.IDProjection;
  import br.nemo.immigrant.ontology.transformaload.ontology.spo.util.mongo.*;
  import br.nemo.immigrant.ontology.entity.spo.*;
  import br.nemo.immigrant.ontology.transformaload.ontology.spo.process.exceptions.ActivityExceptionNotFound;
  import br.nemo.immigrant.ontology.transformaload.ontology.spo.process.exceptions.SpecificProjectProcessExceptionNotFound;


  @Component
  @Transactional
  public class SpecificProjectProcessApplication extends ApplicationAbstract  {

  @Autowired
  private SpecificProjectProcessRepository repository;

  @Autowired
  private ActivityApplication activityApplicationApplication;

  @Autowired
  private SpecificProjectProcessApplication specificprojectprocessApplication;



  public SpecificProjectProcess create(SpecificProjectProcess instance) {
    return this.repository.save(instance);
  }


  public  SpecificProjectProcess create( SpecificProjectProcess instance, String payload) throws Exception, MongoNotFound, ActivityExceptionNotFound,SpecificProjectProcessExceptionNotFound{
    DataSearch activityDataSearch = JsonUtil.retrieve(payload,"activity","activity_id");
    DataSearch specificprojectprocessDataSearch = JsonUtil.retrieve(payload,"specificprojectprocess","specificprojectprocess_id");

    if (activityDataSearch.getElementValue() !=null && activityDataSearch.getElementValue() != 0){
      Document document = this.retrieveDocument(activityDataSearch);
      String internalID = JsonUtil.retrieveInternalID(document.toJson());
      Activity activity = this.activityApplication.retrieveByInternalID(internalID);
      instance.setactivity(activity);

    }

    if (specificprojectprocessDataSearch.getElementValue() !=null && specificprojectprocessDataSearch.getElementValue() != 0){
      Document document = this.retrieveDocument(specificprojectprocessDataSearch);
      String internalID = JsonUtil.retrieveInternalID(document.toJson());
      SpecificProjectProcess specificprojectprocess = this.specificprojectprocessApplication.retrieveByInternalID(internalID);
      instance.setspecificprojectprocess(specificprojectprocess);

    }


    return this.create(instance);
  }



  public SpecificProjectProcess retrieveByExternalID(String externalID) throws TeamExceptionNotFound {
      Optional<IDProjection> result = this.repository.findByExternalId(externalID);

      IDProjection projection = result.orElseThrow(() -> new TeamExceptionNotFound(externalID));

      return createInstance(projection);
  }

  public SpecificProjectProcess retrieveByInternalID(String internalID) throws TeamExceptionNotFound {
      Optional<IDProjection> result = this.repository.findByInternalId(internalID);

      IDProjection projection = result.orElseThrow(() -> new TeamExceptionNotFound(internalID));

      return createInstance(projection);
  }

  private SpecificProjectProcess createInstance(IDProjection projection){
      return  Team.builder().id(projection.getId()).externalId(projection.getExternalId()).internalId(projection.getInternalId()).name(projection.getName()).build();
  }

  public Boolean exists (String internalID){
      return this.repository.existsByInternalId(internalID);
  }

}
