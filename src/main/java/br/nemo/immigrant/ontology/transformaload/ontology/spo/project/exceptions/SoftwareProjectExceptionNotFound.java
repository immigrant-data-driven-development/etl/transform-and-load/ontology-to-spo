  package br.nemo.immigrant.ontology.transformaload.ontology.spo.project.exceptions;

  public class SoftwareProjectExceptionNotFound extends RuntimeException{

    public SoftwareProjectExceptionNotFound(String message) {
        super(message);
    }
}

