
package br.nemo.immigrant.ontology.transformaload.ontology.spo.stakeholder.listeners;

import br.nemo.immigrant.ontology.transformaload.ontology.spo.stakeholder.models.ProjectStakeholder;
import br.nemo.immigrant.ontology.transformaload.ontology.spo.stakeholder.services.ProjectStakeholderService;
import br.nemo.immigrant.ontology.transformaload.ontology.spo.stakeholder.filters.Activity2SPOActivityFilter;
import br.nemo.immigrant.ontology.transformaload.ontology.spo.stakeholder.mappers.Activity2SPOActivityMapper;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.apache.kafka.clients.consumer.ConsumerRecord;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.stereotype.Service;
import org.springframework.kafka.core.KafkaTemplate;

@Slf4j
@RequiredArgsConstructor
//@Service
public class Activity2SPOActivityListener {


    @Autowired
    private Activity2SPOActivityFilter filter;

    @Autowired
    private Activity2SPOActivityMapper mapper;

    @Autowired
    private ProjectStakeholderService service;

    private final KafkaTemplate<String, String> kafkaTemplate;


    @KafkaListener(topics = "ontology.*.projectstakeholder", groupId = "activity2spoactivity-group", concurrency = "2")
    public void consume(ConsumerRecord<String, String> payload) {

        try{


            String data = payload.value();

            if (filter.isValid(data)){

                this.service.process(payload, mapper);
            }

        }
        catch (SoftwareProjectExceptionNotFound e){
            log.error(e.getMessage());
            kafkaTemplate.send("ontology.*", payload.value());

        }

        catch (Exception e ){
            log.error(e.getMessage());
        }
    }
}
