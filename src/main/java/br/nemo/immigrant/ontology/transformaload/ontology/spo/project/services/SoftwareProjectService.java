
package br.nemo.immigrant.ontology.transformaload.ontology.spo.project.services;

import br.nemo.immigrant.ontology.entity.spo.*;
import br.nemo.immigrant.ontology.transformaload.ontology.spo.project.applications.SoftwareProjectApplication;
import br.nemo.immigrant.ontology.transformaload.ontology.spo.project.mappers.Project2SPOProjectMapper;
import br.nemo.immigrant.ontology.transformaload.ontology.spo.util.Mapper;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.apache.kafka.clients.consumer.ConsumerRecord;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

@Slf4j
@RequiredArgsConstructor
@Component
public class SoftwareProjectService {


    @Autowired
    private SoftwareProjectApplication application;

    public void process(ConsumerRecord<String, String> payload,Mapper<SoftwareProject> mapper) throws Exception {


        SoftwareProject instance = mapper.map(payload.value());
        Boolean exists = application.exists(instance.getInternalId());
        if (!exists){
            application.create (instance);

        }

    }

}
