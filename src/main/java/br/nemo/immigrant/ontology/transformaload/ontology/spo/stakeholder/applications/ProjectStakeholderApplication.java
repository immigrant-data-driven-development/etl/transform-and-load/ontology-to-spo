  package br.nemo.immigrant.ontology.transformaload.ontology.spo.stakeholder.applications;

  import br.nemo.immigrant.ontology.transformaload.ontology.spo.util.ApplicationAbstract;
  import org.springframework.stereotype.Component;
  import org.springframework.transaction.annotation.Transactional;
  import java.util.Optional;
  import org.springframework.beans.factory.annotation.Autowired;
  import org.bson.Document;
  import br.nemo.immigrant.ontology.entity.eo.teams.repositories.projections.IDProjection;
  import br.nemo.immigrant.ontology.transformaload.ontology.spo.util.mongo.*;
  import br.nemo.immigrant.ontology.entity.spo.*;
  @Component
  @Transactional
  public class ProjectStakeholderApplication extends ApplicationAbstract  {

  @Autowired
  private ProjectStakeholderRepository repository;

  @Autowired
  private SoftwareProjectApplication softwareprojectApplication;



  public ProjectStakeholder create(ProjectStakeholder instance) {
    return this.repository.save(instance);
  }


  public  ProjectStakeholder create( ProjectStakeholder instance, String payload) throws Exception, MongoNotFound, SoftwareProjectExceptionNotFound{
    DataSearch softwareprojectDataSearch = JsonUtil.retrieve(payload,"softwareproject","softwareproject_id");

    if (softwareprojectDataSearch.getElementValue() !=null && softwareprojectDataSearch.getElementValue() != 0){
      Document document = this.retrieveDocument(softwareprojectDataSearch);
      String internalID = JsonUtil.retrieveInternalID(document.toJson());
      SoftwareProject softwareproject = this.softwareprojectApplication.retrieveByInternalID(internalID);
      instance.setsoftwareproject(softwareproject);

    }


    return this.create(instance);
  }



  public ProjectStakeholder retrieveByExternalID(String externalID) throws TeamExceptionNotFound {
      Optional<IDProjection> result = this.repository.findByExternalId(externalID);

      IDProjection projection = result.orElseThrow(() -> new TeamExceptionNotFound(externalID));

      return createInstance(projection);
  }

  public ProjectStakeholder retrieveByInternalID(String internalID) throws TeamExceptionNotFound {
      Optional<IDProjection> result = this.repository.findByInternalId(internalID);

      IDProjection projection = result.orElseThrow(() -> new TeamExceptionNotFound(internalID));

      return createInstance(projection);
  }

  private ProjectStakeholder createInstance(IDProjection projection){
      return  Team.builder().id(projection.getId()).externalId(projection.getExternalId()).internalId(projection.getInternalId()).name(projection.getName()).build();
  }

  public Boolean exists (String internalID){
      return this.repository.existsByInternalId(internalID);
  }

}
