
package br.nemo.immigrant.ontology.transformaload.ontology.spo.process.services;

import br.nemo.immigrant.ontology.entity.spo.*;
import br.nemo.immigrant.ontology.transformaload.ontology.spo.process.applications.ActivityApplication;
import br.nemo.immigrant.ontology.transformaload.ontology.spo.process.mappers.Activity2SPOActivityMapper;
import br.nemo.immigrant.ontology.transformaload.ontology.spo.util.Mapper;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.apache.kafka.clients.consumer.ConsumerRecord;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

@Slf4j
@RequiredArgsConstructor
@Component
public class ActivityService {


    @Autowired
    private ActivityApplication application;

    public void process(ConsumerRecord<String, String> payload,Mapper<Activity> mapper) throws  ArtifactExceptionNotFound,ArtifactExceptionNotFound,ArtifactExceptionNotFound,ArtifactExceptionNotFound,ActivityExceptionNotFound, Exception{


        Activity instance = mapper.map(payload.value());
        Boolean exists = application.exists(instance.getInternalId());
        if (!exists){
            application.create (instance, payload.value());

        }

    }

}
