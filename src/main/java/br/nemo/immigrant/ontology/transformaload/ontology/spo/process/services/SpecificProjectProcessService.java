
package br.nemo.immigrant.ontology.transformaload.ontology.spo.process.services;

import br.nemo.immigrant.ontology.entity.spo.*;
import br.nemo.immigrant.ontology.transformaload.ontology.spo.process.applications.SpecificProjectProcessApplication;
import br.nemo.immigrant.ontology.transformaload.ontology.spo.process.mappers.SpecificProjectProcess2SPOSpecificProjectProcessMapper;
import br.nemo.immigrant.ontology.transformaload.ontology.spo.util.Mapper;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.apache.kafka.clients.consumer.ConsumerRecord;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

@Slf4j
@RequiredArgsConstructor
@Component
public class SpecificProjectProcessService {


    @Autowired
    private SpecificProjectProcessApplication application;

    public void process(ConsumerRecord<String, String> payload,Mapper<SpecificProjectProcess> mapper) throws Exception, ActivityExceptionNotFound,SpecificProjectProcessExceptionNotFound{


        SpecificProjectProcess instance = mapper.map(payload.value());
        Boolean exists = application.exists(instance.getInternalId());
        if (!exists){
            application.create (instance, payload.value());

        }

    }

}
