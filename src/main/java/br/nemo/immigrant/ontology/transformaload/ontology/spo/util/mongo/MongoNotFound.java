package br.nemo.immigrant.ontology.transformaload.ontology.spo.util.mongo;

public class MongoNotFound extends RuntimeException{

  public MongoNotFound(String message) {
      super(message);
  }
}
