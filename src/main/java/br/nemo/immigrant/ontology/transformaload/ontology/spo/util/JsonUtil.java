package br.nemo.immigrant.ontology.transformaload.ontology.spo.util;
import br.nemo.immigrant.ontology.transformaload.ontology.spo.util.mongo.DataSearch;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.extern.slf4j.Slf4j;


@Slf4j
public class JsonUtil {

    private static JsonNode create (String payload) throws JsonProcessingException {
        ObjectMapper objectMapper = new ObjectMapper();
        return objectMapper.readTree(payload);
    }

    public static DataSearch retrieve (String payload, String table, String elementName) throws JsonProcessingException {
        JsonNode rootNode = create(payload);
        Long elementValue = rootNode.path("payload").path("after").path(elementName).asLong();
        //String table = rootNode.path("payload").path("source").path("table").asText();
        String database = rootNode.path("payload").path("source").path("db").asText();

        return DataSearch.builder().elementValue(elementValue).table(table).database(database).build();
    }

    public static String retrieveInternalID (String payload) throws JsonProcessingException {
        JsonNode rootNode = create(payload);
        return rootNode.path("payload").path("after").path("internal_id").asText();
    }

}
