  package br.nemo.immigrant.ontology.transformaload.ontology.spo.process.applications;

  import br.nemo.immigrant.ontology.transformaload.ontology.spo.util.ApplicationAbstract;
  import br.nemo.immigrant.ontology.entity.spo.*;
  import org.springframework.stereotype.Component;
  import org.springframework.transaction.annotation.Transactional;
  import java.util.Optional;
  import org.springframework.beans.factory.annotation.Autowired;
  import org.bson.Document;
  import br.nemo.immigrant.ontology.entity.eo.teams.repositories.projections.IDProjection;
  import br.nemo.immigrant.ontology.transformaload.ontology.spo.util.mongo.*;
  import br.nemo.immigrant.ontology.transformaload.ontology.spo.artifact.exceptions.ArtifactExceptionNotFound;
  import br.nemo.immigrant.ontology.transformaload.ontology.spo.process.exceptions.ActivityExceptionNotFound;
  @Component
  @Transactional
  public class ActivityApplication extends ApplicationAbstract  {

  @Autowired
  private ActivityRepository repository;

  @Autowired
  private ArtifactApplication artifactApplication;



  public Activity create(Activity instance) {
    return this.repository.save(instance);
  }


  public  Activity create( Activity instance, String payload) throws Exception, MongoNotFound,ArtifactExceptionNotFound,ActivityExceptionNotFound{
    DataSearch artifactDataSearch = JsonUtil.retrieve(payload,"artifact","artifact_id");
    DataSearch activityDataSearch = JsonUtil.retrieve(payload,"activity","activity_id");

    if (artifactDataSearch.getElementValue() !=null && artifactDataSearch.getElementValue() != 0){
      Document document = this.retrieveDocument(artifactDataSearch);
      String internalID = JsonUtil.retrieveInternalID(document.toJson());
      Artifact artifact = this.artifactApplication.retrieveByInternalID(internalID);
      instance.setartifact(artifact);

    }

    if (activityDataSearch.getElementValue() !=null && activityDataSearch.getElementValue() != 0){
      Document document = this.retrieveDocument(activityDataSearch);
      String internalID = JsonUtil.retrieveInternalID(document.toJson());
      Activity activity = this.activityApplication.retrieveByInternalID(internalID);
      instance.setactivity(activity);

    }


    return this.create(instance);
  }



  public Activity retrieveByExternalID(String externalID) throws TeamExceptionNotFound {
      Optional<IDProjection> result = this.repository.findByExternalId(externalID);

      IDProjection projection = result.orElseThrow(() -> new TeamExceptionNotFound(externalID));

      return createInstance(projection);
  }

  public Activity retrieveByInternalID(String internalID) throws TeamExceptionNotFound {
      Optional<IDProjection> result = this.repository.findByInternalId(internalID);

      IDProjection projection = result.orElseThrow(() -> new TeamExceptionNotFound(internalID));

      return createInstance(projection);
  }

  private Activity createInstance(IDProjection projection){
      return  Team.builder().id(projection.getId()).externalId(projection.getExternalId()).internalId(projection.getInternalId()).name(projection.getName()).build();
  }

  public Boolean exists (String internalID){
      return this.repository.existsByInternalId(internalID);
  }

}
