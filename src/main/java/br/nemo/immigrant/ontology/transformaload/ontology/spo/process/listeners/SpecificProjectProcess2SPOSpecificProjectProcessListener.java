
package br.nemo.immigrant.ontology.transformaload.ontology.spo.process.listeners;

import br.nemo.immigrant.ontology.entity.spo.*;
import br.nemo.immigrant.ontology.transformaload.ontology.spo.process.services.SpecificProjectProcessService;
import br.nemo.immigrant.ontology.transformaload.ontology.spo.process.filters.SpecificProjectProcess2SPOSpecificProjectProcessFilter;
import br.nemo.immigrant.ontology.transformaload.ontology.spo.process.mappers.SpecificProjectProcess2SPOSpecificProjectProcessMapper;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.apache.kafka.clients.consumer.ConsumerRecord;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.stereotype.Service;
import org.springframework.kafka.core.KafkaTemplate;

@Slf4j
@RequiredArgsConstructor
//@Service
public class SpecificProjectProcess2SPOSpecificProjectProcessListener {


    @Autowired
    private SpecificProjectProcess2SPOSpecificProjectProcessFilter filter;

    @Autowired
    private SpecificProjectProcess2SPOSpecificProjectProcessMapper mapper;

    @Autowired
    private SpecificProjectProcessService service;

    private final KafkaTemplate<String, String> kafkaTemplate;


    @KafkaListener(topics = "ontology.*.specificprojectprocess", groupId = "specificprojectprocess2spospecificprojectprocess-group", concurrency = "2")
    public void consume(ConsumerRecord<String, String> payload) {

        try{


            String data = payload.value();

            if (filter.isValid(data)){

                this.service.process(payload, mapper);
            }

        }
        catch (ActivityExceptionNotFound e){
            log.error(e.getMessage());
            kafkaTemplate.send("ontology.*", payload.value());

        }

        catch (SpecificProjectProcessExceptionNotFound e){
            log.error(e.getMessage());
            kafkaTemplate.send("ontology.*", payload.value());

        }

        catch (Exception e ){
            log.error(e.getMessage());
        }
    }
}
