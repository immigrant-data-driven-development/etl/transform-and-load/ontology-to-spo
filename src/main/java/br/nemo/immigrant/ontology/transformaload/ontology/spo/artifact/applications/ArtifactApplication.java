  package br.nemo.immigrant.ontology.transformaload.ontology.spo.artifact.applications;

  import br.nemo.immigrant.ontology.transformaload.ontology.spo.util.ApplicationAbstract;
  import br.nemo.immigrant.ontology.entity.spo.*;
  import org.springframework.stereotype.Component;

  import org.springframework.transaction.annotation.Transactional;
  import java.util.Optional;
  import org.springframework.beans.factory.annotation.Autowired;
  import org.bson.Document;
  import br.nemo.immigrant.ontology.entity.eo.teams.repositories.projections.IDProjection;
  import br.nemo.immigrant.ontology.transformaload.ontology.spo.util.mongo.*;
  import br.nemo.immigrant.ontology.transformaload.ontology.spo.artifact.exceptions.ArtifactExceptionNotFound;
  @Component
  @Transactional
  public class ArtifactApplication extends ApplicationAbstract  {

  @Autowired
  private ArtifactRepository repository;

  @Autowired
  private ArtifactApplication artifactApplication;



  public Artifact create(Artifact instance) {
    return this.repository.save(instance);
  }


  public  Artifact create( Artifact instance, String payload) throws  ArtifactExceptionNotFound, Exception, MongoNotFound{
    DataSearch artifactDataSearch = JsonUtil.retrieve(payload,"artifact","artifact_id");

    if (artifactDataSearch.getElementValue() !=null && artifactDataSearch.getElementValue() != 0){
      Document document = this.retrieveDocument(artifactDataSearch);
      String internalID = JsonUtil.retrieveInternalID(document.toJson());
      Artifact artifact = this.artifactApplication.retrieveByInternalID(internalID);
      instance.setartifact(artifact);

    }


    return this.create(instance);
  }



  public Artifact retrieveByExternalID(String externalID) throws TeamExceptionNotFound {
      Optional<IDProjection> result = this.repository.findByExternalId(externalID);

      IDProjection projection = result.orElseThrow(() -> new TeamExceptionNotFound(externalID));

      return createInstance(projection);
  }

  public Artifact retrieveByInternalID(String internalID) throws TeamExceptionNotFound {
      Optional<IDProjection> result = this.repository.findByInternalId(internalID);

      IDProjection projection = result.orElseThrow(() -> new TeamExceptionNotFound(internalID));

      return createInstance(projection);
  }

  private Artifact createInstance(IDProjection projection){
      return  Team.builder().id(projection.getId()).externalId(projection.getExternalId()).internalId(projection.getInternalId()).name(projection.getName()).build();
  }

  public Boolean exists (String internalID){
      return this.repository.existsByInternalId(internalID);
  }

}
