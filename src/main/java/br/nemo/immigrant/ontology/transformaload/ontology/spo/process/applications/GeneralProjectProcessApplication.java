  package br.nemo.immigrant.ontology.transformaload.ontology.spo.process.applications;

  import br.nemo.immigrant.ontology.transformaload.ontology.spo.util.ApplicationAbstract;
  import org.springframework.stereotype.Component;
  import org.springframework.transaction.annotation.Transactional;
  import java.util.Optional;
  import org.springframework.beans.factory.annotation.Autowired;
  import org.bson.Document;
  import br.nemo.immigrant.ontology.entity.eo.teams.repositories.projections.IDProjection;
  import br.nemo.immigrant.ontology.transformaload.ontology.spo.util.mongo.*;
  import br.nemo.immigrant.ontology.entity.spo.*;
  import br.nemo.immigrant.ontology.transformaload.ontology.spo.project.exceptions.SoftwareProjectExceptionNotFound;
  import br.nemo.immigrant.ontology.transformaload.ontology.spo.process.exceptions.SpecificProjectProcessExceptionNotFound;
  import br.nemo.immigrant.ontology.transformaload.ontology.spo.process.exceptions.GeneralProjectProcessExceptionNotFound;
  @Component
  @Transactional
  public class GeneralProjectProcessApplication extends ApplicationAbstract  {

  @Autowired
  private GeneralProjectProcessRepository repository;

  @Autowired
  private SoftwareProjectApplication softwareprojectApplicationApplication;

  @Autowired
  private SpecificProjectProcessApplication specificprojectprocessApplicationApplication;

  @Autowired
  private GeneralProjectProcessApplication generalprojectprocessApplication;



  public GeneralProjectProcess create(GeneralProjectProcess instance) {
    return this.repository.save(instance);
  }


  public  GeneralProjectProcess create( GeneralProjectProcess instance, String payload) throws Exception, MongoNotFound, SoftwareProjectExceptionNotFound,SpecificProjectProcessExceptionNotFound,GeneralProjectProcessExceptionNotFound{
    DataSearch softwareprojectDataSearch = JsonUtil.retrieve(payload,"softwareproject","softwareproject_id");
    DataSearch specificprojectprocessDataSearch = JsonUtil.retrieve(payload,"specificprojectprocess","specificprojectprocess_id");
    DataSearch generalprojectprocessDataSearch = JsonUtil.retrieve(payload,"generalprojectprocess","generalprojectprocess_id");

    if (softwareprojectDataSearch.getElementValue() !=null && softwareprojectDataSearch.getElementValue() != 0){
      Document document = this.retrieveDocument(softwareprojectDataSearch);
      String internalID = JsonUtil.retrieveInternalID(document.toJson());
      SoftwareProject softwareproject = this.softwareprojectApplication.retrieveByInternalID(internalID);
      instance.setsoftwareproject(softwareproject);

    }

    if (specificprojectprocessDataSearch.getElementValue() !=null && specificprojectprocessDataSearch.getElementValue() != 0){
      Document document = this.retrieveDocument(specificprojectprocessDataSearch);
      String internalID = JsonUtil.retrieveInternalID(document.toJson());
      SpecificProjectProcess specificprojectprocess = this.specificprojectprocessApplication.retrieveByInternalID(internalID);
      instance.setspecificprojectprocess(specificprojectprocess);

    }

    if (generalprojectprocessDataSearch.getElementValue() !=null && generalprojectprocessDataSearch.getElementValue() != 0){
      Document document = this.retrieveDocument(generalprojectprocessDataSearch);
      String internalID = JsonUtil.retrieveInternalID(document.toJson());
      GeneralProjectProcess generalprojectprocess = this.generalprojectprocessApplication.retrieveByInternalID(internalID);
      instance.setgeneralprojectprocess(generalprojectprocess);

    }


    return this.create(instance);
  }



  public GeneralProjectProcess retrieveByExternalID(String externalID) throws TeamExceptionNotFound {
      Optional<IDProjection> result = this.repository.findByExternalId(externalID);

      IDProjection projection = result.orElseThrow(() -> new TeamExceptionNotFound(externalID));

      return createInstance(projection);
  }

  public GeneralProjectProcess retrieveByInternalID(String internalID) throws TeamExceptionNotFound {
      Optional<IDProjection> result = this.repository.findByInternalId(internalID);

      IDProjection projection = result.orElseThrow(() -> new TeamExceptionNotFound(internalID));

      return createInstance(projection);
  }

  private GeneralProjectProcess createInstance(IDProjection projection){
      return  Team.builder().id(projection.getId()).externalId(projection.getExternalId()).internalId(projection.getInternalId()).name(projection.getName()).build();
  }

  public Boolean exists (String internalID){
      return this.repository.existsByInternalId(internalID);
  }

}
