package br.nemo.immigrant.ontology.transformaload.ontology.spo.util.mongo;

import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;
import org.bson.Document;
import org.springframework.data.mongodb.core.query.BasicQuery;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoTemplate;

@Component
@Transactional
@Slf4j
public class MongoApplication {

    @Autowired
    private MongoTemplate mongoTemplate;

    public static final String COLLECTION = "input";
    public List<Document>  find(String tableName, Long id, String database) throws MongoNotFound {

        String jsonQuery = "{$and:[{\"payload.source.table\":\"{TABLE}\"},{\"payload.after.id\":{ID}},{\"payload.source.db\":\"{DATABASE}\"}]}";

        jsonQuery = jsonQuery.replace("{TABLE}", tableName);
        jsonQuery = jsonQuery.replace("{ID}", String.valueOf(id));
        jsonQuery = jsonQuery.replace("{DATABASE}", database);

        log.info(" jsonQuery: {}", jsonQuery);

        BasicQuery query = new BasicQuery(jsonQuery);
        List<Document> documents = mongoTemplate.find(query, Document.class, COLLECTION);

        if (documents.isEmpty()) throw new MongoNotFound(tableName+"-"+id+"-"+database);

        return documents;
    }
}
