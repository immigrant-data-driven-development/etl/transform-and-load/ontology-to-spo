  package br.nemo.immigrant.ontology.transformaload.ontology.spo.stakeholder.exceptions;

  public class ProjectStakeholderExceptionNotFound extends RuntimeException{

    public ProjectStakeholderExceptionNotFound(String message) {
        super(message);
    }
}

