
# ontology2spo
## 🚀 Goal

Mapping Ontology Based on SPO to SPO Database

## 📕 Domain Documentation

Domain documentation can be found [here](./docs/README.md)

## ⚙️ Requirements

1. Postgresql
2. Java 17
3. Maven

## ⚙️ Stack

1. Spring Boot 3.0
2. Spring Kafka


## 🔧 Install

TODO

```bash
mvn Spring-boot:run
```


## ✒️ Team

* **[](__«SKIP^NEW^LINE^IF^EMPTY»__)**

## 📕 Literature

